#include "Crucianfish.hpp"

#include <string>

int main()
{
    const std::string key{"635266556A586E5A7234753778214125"};

    crucianfish::Crucianfish cipher{key};

    cipher.encodeFile("input_data.txt", "clipher_data.txt");
    cipher.decodeFile("clipher_data.txt", "output_data.txt");

    return 0;
}