# Crucianfish Algorithm - Karaś Algorytm

### Crucianfish jest to metoda szyfrowania danych zaprojektowana przez Marcina Karasiewicza w 2021 jako alternatywa dla Blowfish'a.

#### Parametry:

* Wielkość bloku wejściowego: 64-bity
* Wielkość klucza: 128-bits
* Liczba pod-kluczy: 16
* Liczba rund: 16
* Liczba s-box'ów: 4 (każdy posiada 256 32-bitowych słow)

### Crucianfish algorytm szyfrowania kroki:

#### 0. Generowanie pod-kluczy:
Przed rozpoczęciem właściwego szyfrowania generowana jest 16-sto elementowa tablica zawierająca pod-klucze.

128-bitowy klucz dzielimy na 4 32-bitowe słowa.
Następnie każde ze słow jest poddawane operacji XOR dla kolejnych elementów tablicy która składa się z 4 liczb a są to:
* e
* pi
* egamma
* phi

Proces generowania pod-kluczy obrazuje poniższy rysunek:\
![Encryption](doc/Key.png)

Kod generujący klucze:
```cpp
void Crucianfish::generateSubKeys(const std::string& key)
{
    constexpr uint8_t charsPerByteInHexKey{2};
    constexpr uint32_t pi{0x3243f6a8}; // first 8 digits of pi in hex
    constexpr uint32_t e{0x2b7e1516}; // first 8 digits of e in hex"
    constexpr uint32_t phi{0x19e3779b}; // first 8 digits of Golden Ratio in hex"
    constexpr uint32_t lemniscate{0x53e7e53e}; // first 8 digits of lemniscate in hex"

    std::array<uint32_t, 4> splittedKey{};
    for (size_t i = 0; i < splittedKey.size(); ++i)
    {
        splittedKey.at(i) = stoi(key.substr(i * charsPerByteInHexKey * sizeof(uint32_t), 8), 0, 16);
    }

    for (size_t i = 0, j = 0; i < subkeys.size(); i += 4, ++j)
    {
        subkeys.at(i) = splittedKey.at(j) ^ pi;
        subkeys.at(i + 1) = splittedKey.at(j) ^ e;
        subkeys.at(i + 2) = splittedKey.at(j) ^ phi;
        subkeys.at(i + 3) = splittedKey.at(j) ^ lemniscate;
    }
}
```
#### 1. Szyfrowanie:

64-bitowy blok jest dzielony na 2 32-bitowe słowa które następnie są 16-krotnie obrabiany podczas rundy.\
Proces obróbki bloku w rundzie zostanie opisany poniżej.

Cały proces szyfrowania obrazuje poniższy rysunek.\
![Encryption](doc/Encryption.png)

Kod wykonujący szyfrowanie bloku:
```cpp
void Crucianfish::encode()
{
    constexpr uint64_t first32BitMask{0xFFFFFFFF00000000};
    constexpr uint64_t last32BitMask{0xFFFFFFFF};

    while (not blocks.empty())
    {
        const auto currentBlock{blocks.front()};
        blocks.pop();

        uint32_t left = (currentBlock & first32BitMask) >> 32;
        uint32_t right = currentBlock & last32BitMask;

        for (size_t i = 0; i < rounds; ++i)
        {
            auto tmp = left;
            left = f(left ^ subkeys.at(i)) ^ right;
            right = tmp;
        }
        const uint64_t clipherBlock{((uint64_t)left << 32) + right};

        clipherBlocks.push(clipherBlock);

        ++encodedBlock;
    }
}
```

#### 1.1 Runda:

Podczas jednej rundy przyjmujemy na wejściu 2 32-bitowe słowa.
Lewe jest poddawane operacji XOR z podkluczem o indeksie danej rundy.
Następnie wywoływana jest funkcja F którą opiszę później.
Przetworzone lewe słowo przez funkcje F jest poddane operacji XOR ze słowem prawym
a następnie zapisane jak słowo lewe.
Natomiast wcześniej zapamiętane słowo lewe przed modyfikacjami zostaje zapisane jako słowo lewe. 

Proces obróbki bloku w rundzie obrazuje poniższy rysunek.\
![Encryption](doc/round.png)

#### 1.2 Funkcja F:

Funkcja F na wejściu przyjmuje 32-bitowe słowo.
Dzieli je na 4 8-bitowe słowa które następnie stają się indeksami do S-Box'a.
Każdy S-Box zwraca 32-bitowe słowo spod danego indeksu.
Następnie Pierwsze i Drugie słowo jest poddawane operacji XOR tak sami Trzecie i Czwarte.
Nastepnie słowa wytnikające z poprzednich operacji jest dodawane do siebie tworzą wyjściowe 32-bitowe słowo.

Schemat przepływu funkcji F.\
![Encryption](doc/F.png)

Kod realizujący funkcję F:
```cpp
inline uint32_t Crucianfish::f(const uint32_t left)
{
    const uint8_t x1 = left;
    const uint8_t x2 = left >> 8;
    const uint8_t x3 = left >> 16;
    const uint8_t x4 = left >> 24;

    return (sbox[0][x1] ^ sbox[1][x2]) + (sbox[2][x3] ^ sbox[3][x4]);
}
```
W ten sposób powstają 64-bitowe zaszyfrowane słowa.

#### 2. Odszyfrowanie:

Proces wygląda dokładnie tak jak szyfrowanie, przy czym tablica podkluczy posiada odwróconą kolejność, oraz słowa wejściowe są zamienione pozycjami tzm. słowo wcześniej prawe staje się lewym i odwrotnie.

#### 3. Wydajność

Szyfrowanie
* runtime 3.35033s
* operation time 0.112693s
* Processed blocks 2021276
* Performacne 17936079 block/s
* Performacne 143488632 B/s
* Performacne 140125 kB/s
* Performacne 136 MB/s

Odszyfrowanie
* runtime 0.404673s
* operation time 0.125082s
* Processed blocks 2021276
* Performacne 16159671 block/s
* Performacne 129277368 B/s
* Performacne 126247 kB/s
* Performacne 123 MB/s

Jak widać wydajność jest na przyzwoitym pozimie pomimo braku optymalizacji.

### Budowanie

#### Uwaga!!! Wymagany G++9
```bash
g++ -Wall -Wextra -std=c++2a  -lstdc++ Crucianfish.cpp -o Crucianfish -O3
```